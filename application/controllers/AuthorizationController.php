<?php

class AuthorizationController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        session_start();
        session_destroy();

        session_start();

            //start checking for authorization
            $staffIc_encode = $this->_getParam('ic', '');
            $staffIc = base64_decode($staffIc_encode);

            $_SESSION["ic"] = $staffIc;
            // $staffIc = "830226115170";

            $myfile = fopen("../application/configs/authorization.txt", "r") or die("Unable to open file!");
            $auth_str = fread($myfile, filesize("../application/configs/authorization.txt"));
            fclose($myfile);

            $auth_arr = explode(",", $auth_str);


            foreach ($auth_arr as $auth) {
                if ($auth == $staffIc) {
                    $check_auth = "true";

                    break;
                } else {
                    $check_auth = "false";
                }
            }


        if ($check_auth == "true") {
            $redirectUrl = '/qr-management/index?staffIc=' . urlencode($staffIc);
            $this->_redirect($redirectUrl);
        }else{
            $this->_redirect('/authorization/unauthorize');
            $msg =  "Sorry you are not authorize to access this page.";
            $this->view->msg = $msg;
        }

    }

    public function unauthorizeAction(){


    }

    public function sessionExpiredAction(){
        session_start();
        session_destroy();

    }



}
