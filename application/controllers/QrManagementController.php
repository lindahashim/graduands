<?php

class QrManagementController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */

    }

    public function indexAction()
    {
        $this->view->title = "Graduation List";
        session_start();

        $request = $this->getRequest();
        $staffIc = $request->getParam('staffIc');

        //echo $staffIc;exit;

        $_SESSION["ic"]=$staffIc;

        $this->checkingSession();

        $convo = new ConvoListing();

            if ($this->_request->isPost()) {
                $formData = $this->_request->getPost();

//                $date = $this->_getParam('senate_date', '');
//                $degreeid = $this->_getParam('degree', '');
//                $studIc = $this->_getParam('studIc', '');
//                $progid = $this->_getParam('program', '');

                $date = $formData['senate_date'];
                $progid = $formData['program'];
                $degreeid = $formData['degree'];
                $studIc = $formData['studIc'];

                $_SESSION["date"] = $date;
                $_SESSION["progid"] = $progid;
                $_SESSION["degreeid"] = $degreeid;
                $_SESSION["studIc"] = $studIc;
                unset($_SESSION["sts"]);


            } else {

                /*$date = $this->_getParam('senate_date', '');
                $degreeid = $this->_getParam('degree', '');
                $studIc = $this->_getParam('studIc', '');
                $progid = $this->_getParam('program', '');
                $sts = $this->_getParam('sts', '');
                $auth = $this->_getParam('auth', '');*/

                $date = $_SESSION["date"];
                $degreeid = $_SESSION["degreeid"];
                $studIc = $_SESSION["studIc"];
                $progid = $_SESSION["progid"];
                $sts = $_SESSION["sts"];


                //$auth = $this->_getParam('auth', '');

            }


            if ($date) {
                $getConvoList = $convo->getConvoListing($date, $progid, $degreeid, $studIc);
            }

            $this->view->convoList = $getConvoList;
            $this->view->senate_date = $date;
            $this->view->degreeid = $degreeid;
            $this->view->progid = $progid;
            $this->view->studIc = $studIc;
            $this->view->status = $sts;
            unset($_SESSION["sts"]);
            //$this->view->status = $sts;

            //$convo = new ConvoListing();
            $getSenateDate = $convo->getSenateDate();
            $this->view->getDateList = $getSenateDate;

            if ($date) {
                $getPrograms = $convo->getProgramList($date);
                $this->view->getProgramList = $getPrograms;
            }

            if ($date && $progid) {

                $getDegrees = $convo->getDegreeList($date, $progid);
                $this->view->getDegreeList = $getDegrees;
            }

    }

    public function genUrlAction()
    {
        $this->view->title = "Search Result";
        session_start();
        $this->checkingSession();

        $senate_date = $this->_getParam('senate_date', '');
        $degreeid = $this->_getParam('degree', '');
        $studIc = $this->_getParam('studIc', '');
        $progid = $this->_getParam('program', '');

        $this->view->senate_date = $senate_date;
        $this->view->degreeid = $degreeid;
        $this->view->progid = $progid;
        $this->view->studIc = $studIc;

        //$encryption = new Encryption();

        //$sid = 'CGS00292501'.'/'.'811007115441';
        //$sid = $encryption::encode($sid);
        //$base64_decode = $encryption::decode($base64_crypt);
        //$url = 'http://10.0.6.82/graduands/qrgraduan?sid='.$sid;

        $getList = new ConvoListing();
        //echo  $senate_date.'-'.$progid.'-'.$degreeid.'-'.$studIc;exit;
        $getConvoList = $getList->getConvoListingForUpdate('gen-url', $senate_date, $progid, $degreeid, $studIc);

        $result2 = "";
        $i = 0;
        if ($getConvoList) {
            foreach ($getConvoList as $itm) {

                $sid = $itm['CL_STUD_ID'] . '/' . $itm['CL_SCROLL_SERIAL'];

                //encrypt link stud_id/serialno before masukkan dlm link
                //$sid = $encryption->encode($sid);
                $sid = base64_encode($sid);

                //$base64_decode = $encryption::decode($base64_crypt);

                //$url = 'http://10.0.6.82/graduands/qrgraduan?sid=' . $sid;
                $url = 'http://verifyoumgrads.oum.edu.my/graduands/qrgraduan?sid='. $sid;

                $updateItems = array(
                    'CL_SCROLL_URL' => $url
                );
                $getList->updateUrl($updateItems, $itm["CL_ID"]);


            }
        $result2 = "success";

        }
        $_SESSION["sts"] = "success_url";

        $result[] = array(
            'result' => 'success'
        );

        $json = Zend_Json::encode($result);
        echo $json;

        exit;
        //$this->_redirect('/qr-management/index');
    }

    public function genSerialAction()
    {
        $this->view->title = "Generate Serial Number";
        session_start();
        $this->checkingSession();

        $senate_date = $this->_getParam('senate_date', '');
        $degreeid = $this->_getParam('degree', '');
        $studIc = $this->_getParam('studIc', '');
        $progid = $this->_getParam('program', '');

        $convo = new ConvoListing();

        $this->view->senate_date = $senate_date;
        $this->view->degreeid = $degreeid;
        $this->view->progid = $progid;
        $this->view->studIc = $studIc;

        $convo = new ConvoListing();
        $getConvoList = $convo->getConvoListingForUpdate('gen-serial', $senate_date, $progid, $degreeid, $studIc);

        $i = 0;
        if ($getConvoList) {
            foreach ($getConvoList as $itm) {
                $pstud_id = $itm['CL_STUD_ID'];

                //generate new serial no for the student
                $new_serialNo = $convo->genSerialNo($pstud_id);

                $updateItems = array(
                    'CL_SCROLL_SERIAL' => $new_serialNo
                );
                $convo->updateSerialNo($updateItems, $itm["CL_ID"]);


            }


        }
        $_SESSION["sts"] = "success_sn";

        $result[] = array(
            'result' => 'success'
        );

        $json = Zend_Json::encode($result);
        echo $json;

        exit;

       // $this->_redirect('/qr-management/index');

    }

    public function generateExcelAction()
    {
        $this->view->title = "Search Result";

        $today = date('Y-m-d');

        $senate_date = $this->_getParam('senate_date', '');
        $degreeid = $this->_getParam('degree', '');
        $studIc = $this->_getParam('studIc', '');
        $progid = $this->_getParam('program', '');

        $convo = new ConvoListing();
        /*  $getConvoList = $convo->getConvoListing($senate_date,$progid,$degreeid,$studIc);
          $this->view->convoList = $getConvoList;*/
        $this->view->senate_date = $senate_date;
        $this->view->degreeid = $degreeid;
        $this->view->progid = $progid;
        $this->view->studIc = $studIc;

        //$sem = $formData['sem'];
        $getList = new ConvoListing();
        $getConvoList = $getList->getConvoListGenExcel($senate_date, $progid, $degreeid, $studIc);
        $this->view->getData = $getConvoList;


        $filename = "OUM Graduates Senate Date (" . $senate_date . ").xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment; filename=' . $filename);

        //$lblStudentid = "Student ID";
        $lblStudentSerialNo = "Serial Number";
        $lblStudentName = "Student Name";
        $lblDegreeBm = "Degree (Malay)";
        $lblSenateDateBm = "Senate Date (Malay)";
        $lblNotaBm = "Academic Status (Malay)";
        $lblDegreeEng = "Degree (Eng)";
        $lblSenateDateEng = "Senate Date (Eng)";
        $lblNotaEng = "Academic Status (Eng)";
        $lblUrl = "URL";


        $header = $this->view->translate('Convocation Listing for Senate Date (' . $senate_date . ')');
        $header = $header . "\n\n";
        echo $header;

//        $header1 = "List of Convocation Listing \n\n";
//        echo $header1;

        // column title/header
        $contents1 = "No \t $lblStudentSerialNo \t $lblStudentName \t $lblDegreeBm \t $lblSenateDateBm \t $lblNotaBm \t $lblDegreeEng \t $lblSenateDateEng \t $lblNotaEng \t $lblUrl \n ";
        echo $contents1;

        $i = 1;
        foreach ($getConvoList as $dataList) {

            $serialNo = $dataList['CL_SCROLL_SERIAL'];
            $studentName = $dataList['STUD_NAME'];
            $degree_bm = $dataList['DEGREE_BM'];
            $degree_eng = $dataList['DEGREE_BI'];
            $senate_date_bm = $dataList['SENATE_DATE_BM'];
            $senate_date_eng = $dataList['SENATE_DATE_BI'];
            $nota_bm = $dataList['CL_ACADEMICSTATUS'];
            $nota_eng = $dataList['CL_ACADEMICSTATUS_ENG'];
            $scrollurl = $dataList['CL_SCROLL_URL'];


            $contents2 = "$i \t $serialNo \t $studentName  \t $degree_bm \t $senate_date_bm \t $nota_bm \t $degree_eng \t $senate_date_eng \t $nota_eng \t $scrollurl \n";
            echo $contents2;
            $i++;
        }
        exit;

        //}
    }


    public function generateExcelSingleAction()
    {


        $today = date('Y-m-d');

        $cl_id = $this->_getParam('cl_id', '');
        $studId = $this->_getParam('studId', '');

        $convo = new ConvoListing();


        //$sem = $formData['sem'];
        $getList = new ConvoListing();
        $getConvoList = $getList->getConvoListGenExcel('', '', '', '',$cl_id);
        $this->view->getData = $getConvoList;


        $filename = "OUM Graduates Student ID (" . $studId . ").xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment; filename=' . $filename);

        //$lblStudentid = "Student ID";
        $lblStudentSerialNo = "Serial Number";
        $lblStudentName = "Student Name";
        $lblDegreeBm = "Degree (Malay)";
        $lblSenateDateBm = "Senate Date (Malay)";
        $lblNotaBm = "Academic Status (Malay)";
        $lblDegreeEng = "Degree (Eng)";
        $lblSenateDateEng = "Senate Date (Eng)";
        $lblNotaEng = "Academic Status (Eng)";
        $lblUrl = "URL";


        $header = $this->view->translate('Convocation Detail for Student (' . $studId . ')');
        $header = $header . "\n\n";
        echo $header;

        // column title/header
        $contents1 = "No \t $lblStudentSerialNo \t $lblStudentName \t $lblDegreeBm \t $lblSenateDateBm \t $lblNotaBm \t $lblDegreeEng \t $lblSenateDateEng \t $lblNotaEng \t $lblUrl \n ";
        echo $contents1;

        $i = 1;
        foreach ($getConvoList as $dataList) {

            $serialNo = $dataList['CL_SCROLL_SERIAL'];
            $studentName = $dataList['STUD_NAME'];
            $degree_bm = $dataList['DEGREE_BM'];
            $degree_eng = $dataList['DEGREE_BI'];
            $senate_date_bm = $dataList['SENATE_DATE_BM'];
            $senate_date_eng = $dataList['SENATE_DATE_BI'];
            $nota_bm = $dataList['CL_ACADEMICSTATUS'];
            $nota_eng = $dataList['CL_ACADEMICSTATUS_ENG'];
            $scrollurl = $dataList['CL_SCROLL_URL'];


            $contents2 = "$i \t $serialNo \t $studentName  \t $degree_bm \t $senate_date_bm \t $nota_bm \t $degree_eng \t $senate_date_eng \t $nota_eng \t $scrollurl \n";
            echo $contents2;
            $i++;
        }
        exit;

        //}
    }

    public function ajaxProgramListAction()
    {

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $senate_date = $this->_getParam('senate_date', '');

        $convo = new ConvoListing();

        $getPrograms = $convo->getProgramList($senate_date);
        $this->view->getProgramList = $getPrograms;

        $json = Zend_Json::encode($getPrograms);

        $this->view->json = $json;
    }

    public function ajaxDegreeListAction()
    {

        if ($this->getRequest()->isXmlHttpRequest()) {

            $this->_helper->layout->disableLayout();
        }

        $senate_date = $this->_getParam('senate_date', '');
        $progid = $this->_getParam('programid', '');

        $convo = new ConvoListing();

        $getDegrees = $convo->getDegreeList($senate_date, $progid);
        $this->view->getDegreeList = $getDegrees;

        $json = Zend_Json::encode($getDegrees);

        $this->view->json = $json;
    }

    //populate data to dialog box
    public function editScrollInfoAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $this->view->title =  strtoupper("Edit Scroll Information");

        $cl_id = $this->_getParam ( 'cl_id', '' );
        $this->view->cl_id = $cl_id;

        $convo = new ConvoListing();
        $getInfo = $convo->getConvoDataById($cl_id);
        $this->view->getInfo = $getInfo;

    }

    public function genSerialUpdateAction() {
        session_start();
        $this->checkingSession();


        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            $clid = $this->_getParam('e_cl_id', '');
            $today = date('Y-m-d H:i:s');

            $this->view->cl_id = $clid;

            $convo = new ConvoListing();
            $getConvoList = $convo->getConvoDataById($clid);

            $ic = $_SESSION["ic"];

            //get person updated
            $userdata = $convo->getUsername($ic);
            $username = $userdata['EMP_USERNAME'];

            $i = 0;
            if ($getConvoList) {
                foreach ($getConvoList as $itm) {
                    $pstud_id = $itm['STUD_ID'];

                    //generate new serial no for the student
                    $new_serialNo = $convo->genSerialNo($pstud_id);

                    $old_serialNo = $itm['CL_SCROLL_SERIAL'];
                    $old_url = $itm['CL_SCROLL_URL'];

                    //generate new URL
                    $sid = $pstud_id . '/' . $new_serialNo;

                    //encrypt link stud_id/serialno before masukkan dlm link
                    $sid = base64_encode($sid);

                    //$newurl = 'http://10.0.6.82/graduands/qrgraduan?sid=' . $sid;
                    $newurl = 'http://verifyoumgrads.oum.edu.my/graduands/qrgraduan?sid=' . $sid;

                    $item = array(
                        'SSH_CL_ID' => $itm['CL_ID'],
                        'SSH_NEW_SERIAL_NO' => $new_serialNo,
                        'SSH_OLD_SERIAL_NO' => $old_serialNo,
                        'SSH_OLD_URL_SCROLL' => $old_url,
                        'SSH_NEW_URL_SCROLL' => $newurl,
                        'SSH_CREATE_DATE' => $today,
                        'SSH_CREATE_BY' => $username

                    );

                    $convo->insertHistory($item);

                    $convo->updateNewSnURLById($item, $clid);



                }


            }
        }


        $result[] = array(
            'resultid' => $new_serialNo
        );

        $json = Zend_Json::encode($result);
        echo $json;

        exit;


    }

    public function genUrlUpdateAction() {
        session_start();
        $this->checkingSession();


        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            $clid = $this->_getParam('e_cl_id', '');
            $today = date('Y-m-d H:i:s');

            $this->view->cl_id = $clid;

            $convo = new ConvoListing();
            $getConvoList = $convo->getConvoDataById($clid);

            $ic = $_SESSION["ic"];

            //get person updated
            $userdata = $convo->getUsername($ic);
            $username = $userdata['EMP_USERNAME'];

            $i = 0;
            if ($getConvoList) {
                foreach ($getConvoList as $itm) {
                    $pstud_id = $itm['STUD_ID'];

                    $old_serialNo = $itm['CL_SCROLL_SERIAL'];
                    $old_url = $itm['CL_SCROLL_URL'];

                    //generate new URL
                    $sid = $pstud_id . '/' . $old_serialNo;

                    //encrypt link stud_id/serialno before masukkan dlm link
                    $sid = base64_encode($sid);

                    //$newurl = 'http://10.0.6.82/graduands/qrgraduan?sid=' . $sid;
                    $newurl = 'http://verifyoumgrads.oum.edu.my/graduands/qrgraduan?sid=' . $sid;

                    $item = array(
                        'SSH_CL_ID' => $itm['CL_ID'],
                        'SSH_NEW_SERIAL_NO' => $old_serialNo,
                        'SSH_OLD_SERIAL_NO' => $old_serialNo,
                        'SSH_OLD_URL_SCROLL' => $old_url,
                        'SSH_NEW_URL_SCROLL' => $newurl,
                        'SSH_CREATE_DATE' => $today,
                        'SSH_CREATE_BY' => $username

                    );

                    $convo->insertHistory($item);

                    $convo->updateNewUrlById($item, $clid);

                }


            }
        }


        $result[] = array(
            'resultid' => $clid
        );

        $json = Zend_Json::encode($result);
        echo $json;

        exit;


    }

    public function checkingSession(){
        if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 60 * 60)) {
            // last request was more than 1 hour
            session_unset();     // unset $_SESSION variable for the run-time
            session_destroy();   // destroy session data in storage
            $this->_redirect('/authorization/session-expired');
            //echo "Your session already expired. Please access again from the link provided in Staff Portal.";
            //exit;
        }
        $_SESSION['LAST_ACTIVITY'] = time();

        if (!isset($_SESSION["ic"])){
            $this->_redirect('/authorization/session-expired');
        }
    }

    //populate data to dialog box
    public function dateScreenAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $this->view->title =  strtoupper("Edit Date Sirim");

        $senate_date = $this->_getParam('senate_date', '');
        $degreeid = $this->_getParam('degree', '');
        $studIc = $this->_getParam('studIc', '');
        $progid = $this->_getParam('program', '');

        $today = date('Y-m-d H:i:s');
        $this->view->current_date = $today;



    }

    public function updateDateAction()
    {
        session_start();
        $this->checkingSession();


        $senate_date = $this->_getParam('senate_date', '');
        $degreeid = $this->_getParam('degree', '');
        $studIc = $this->_getParam('studIc', '');
        $progid = $this->_getParam('program', '');
        $send_date = $this->_getParam('date', '');


            $convo = new ConvoListing();
            $getConvoList = $convo->getConvoListingForUpdate('update-date', $senate_date, $progid, $degreeid, $studIc);

            $result2 = "";
            $i = 0;
            if ($getConvoList) {
                foreach ($getConvoList as $itm) {

                    $item = array(
                        'CL_SEND_DATE' => $send_date
                    );

                    $convo->updateDateSirimById($item, $itm["CL_ID"]);

                }
               $result2 = "success";

            }

        $result[] = array(
            'result' => $result2
        );

        $json = Zend_Json::encode($result);
        echo $json;

        exit;



    }


}
