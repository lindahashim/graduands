<?php

class QrgraduanController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $this->view->title = "Search Graduands QR";
        //$url = 'http://graduands.local/graduands/qrgraduan?sid=to_PwQKJvsGHaN12zzeeQfk6CrXWkYVvXLyRk2NqRldTm60JAIHT_aCipR9nHvHq';
        //$url = '//graduands.local/graduands/qrgraduan?sid=xE4UoA6mxR94fbsaAH66_paOX_c3JReXxv8Shg2wgD7gfzsT1w70dcLji6dpW1lT';

        //$encryption = new Encryption();

        $get_crypt_sid = $_GET['sid'];

        //$sid = $encryption::decode($get_crypt_sid);
        $sid = base64_decode($get_crypt_sid);

        //$studentid = substr($sid, 0, strpos($sid, '/'));
        //$serialno = substr($sid, strpos($sid, "/") + 1);

        $str_arr = explode ("/", $sid);

        $studentid = $str_arr[0];
        $serialno = $str_arr[1];

        // action body
        $getDetails = new ConvoListing();
        $all = $getDetails->getStudentQR($studentid,$serialno);
        $this->view->details = $all;
        $this->view->serialno = $serialno;
    }


}
