<?php

class IndexController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $this->view->title = "Search Graduands";
        // action body
        $getDetails = new Students();
        $all = $getDetails->getAllStudent();
        $this->view->getAll = $all;
    }
    
    // UPDATE ON 280416
    public function resultAction() {
        $this->view->title = "Search Result";
        // action body
        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            
            $icNum = $formData['icNum'];
            $this->view->icNum = $icNum;

            $getDetails = new Students();
            $getAll = $getDetails->getStudent($icNum);
            $this->view->details = $getAll;

        }
    }
//    public function resultAction() {
//        $this->view->title = "Search Result";
//        // action body
//        if ($this->_request->isPost()) {
//            $formData = $this->_request->getPost();
//
//            $fullName = strtoupper($formData['fullName']);
//            $this->view->fullname = $fullName;
//            $icNum = $formData['icNum'];
//            $this->view->icNum = $icNum;
//
//            $getDetails = new Students();
//            $getAll = $getDetails->getStudent($fullName, $icNum);
//            $this->view->details = $getAll;
//
//        }
//    }

}
