<?php

class Students extends Zend_Db_Table_Abstract {

    protected $_graduate = "VIEW_STUD_GRADUATE";
    protected $_profile = "STUDENT_PROFILE";

//    public function getStudent($fullName = "", $icNum = "") {
//
//        $multidb = Zend_Registry::get("multidb");
//        $this->_db = $multidb->getDb('oracle');
//
//        $sql = $this->_db->select()
//                ->from(array('G' => $this->_graduate), array('SG_STUD_ID', 'SG_STUD_NAME', 'SG_STUD_IC', 'SG_GRAD_YEAR', 'SG_DEGREE', 'SG_PROGRAM', 'SG_INTAKE_YEAR'));
////                ->join(array('P' => $this->_profile), 'G.SG_STUD_ID = P.STUD_ID', array('STUD_ID','STUD_DEGREE', 'STUD_STATUS'));
////                ->where('P.STUD_STATUS = ?', 'GRADUATED');
//
//        if ($fullName != "") {
//            $sql->orWhere('G.SG_STUD_NAME LIKE ?', '%' . $fullName . '%');
//        }
//        if ($icNum != "") {
//            $sql->orWhere('G.SG_STUD_IC LIKE ?', '%' . $icNum . '%');
//        }
//        $sql->order('G.SG_STUD_NAME ASC');
//
////        echo $sql;
//        $result = $this->_db->fetchAll($sql);
//        $this->_db->closeConnection();
//        return $result;
//    }

    // UPDATE ON 280416
    public function getStudent($icNum = "") {

        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $sql = $this->_db->select()
            ->from(array('G' => $this->_graduate), array('SG_STUD_ID', 'SG_STUD_NAME', 'SG_STUD_IC', 'SG_GRAD_YEAR', 'SG_DEGREE', 'SG_PROGRAM', 'SG_INTAKE_YEAR'));
        
        if ($icNum != "") {
            $sql->orWhere('G.SG_STUD_IC = ?',$icNum);
        }
        $sql->order('G.SG_STUD_NAME ASC');

        $result = $this->_db->fetchAll($sql);
        $this->_db->closeConnection();
        return $result;
    }
    
    public function getAllStudent() {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $sql = $this->_db->select()
                ->from($this->_graduate, array('SG_STUD_ID', 'SG_STUD_NAME', 'SG_STUD_IC'))
                ->order('SG_STUD_ID ASC')
                ->limit('3');

//        echo $sql;
        $result = $this->_db->fetchAll($sql);
        $this->_db->closeConnection();
        return $result;
    }

}
