<?php

class ConvoListing extends Zend_Db_Table_Abstract {

    protected $_profile = "STUDENT_PROFILE";
    protected $_convolist = "CONVOCATION_LISTING";
    protected $_degree = "DEGREE_MAIN";


    public function getStudentQR($studid = "",$serialno = "") {

        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $sql = "select upper(stud_name) stud_name, upper(degree_desc_short) degree_bi, upper(degree_desc_long) degree_bm,
                cl_academicstatus, cl_academicstatus_eng,
                to_char(cl_senate_date,'DD')||' '||rtrim(to_char(cl_senate_date,'Month','NLS_DATE_LANGUAGE=malay'))||' '||to_char(cl_senate_date,'YYYY') senate_date_bm,
                to_char(cl_senate_date,'DD')||' '||rtrim(to_char(cl_senate_date,'Month'))||' '||to_char(cl_senate_date,'YYYY') senate_date_bi,
                cl_scroll_serial, cl_scroll_url
                from student_profile, degree_main, convocation_listing
                where stud_degree = degree_id
                and stud_id = cl_stud_id
                and cl_scroll_serial = '$serialno'
                and cl_stud_id = '$studid'";

       /* $sql = $this->_db->select()
            ->from(array('C' => $this->_convolist))
            ->join(array('S' => $this->_profile),'C.CL_STUD_ID = S.STUD_ID', array('STUD_NAME'))
            ->join(array('D' => $this->_degree), 'D.DEGREE_ID = S.STUD_DEGREE',array('DEGREE_ID','DEGREE_DESC_LONG','DEGREE_DESC_SHORT'));
        $sql->where('C.CL_STUD_ID = ?',$studid);
        if($serialno) {
            $sql->where('C.CL_SCROLL_SERIAL = ?', $serialno);
        }*/

        $result = $this->_db->fetchAll($sql);
        $this->_db->closeConnection();
        return $result;
    }

    public function getConvoListing($senate_date = "",$program = "",$degree = "",$stud_ic = "") {

        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $sql1= " select cl_id,cl_sem_grad, stud_id, stud_ic_cur, upper(stud_name) stud_name, stud_program, stud_degree,
                upper(degree_desc_short) degree_desc,
                cl_academicstatus_eng,
                to_number(to_char(cl_senate_date,'DD'))||' '||rtrim(to_char(cl_senate_date,'Month'))||' '||to_char(cl_senate_date,'YYYY') senate_date,
                decode(nvl(cl_scroll_serial,'No'),'No','No','Yes') serial_no_sts,
                decode(nvl(cl_scroll_url,'No'),'No','No','Yes') scroll_url_sts,cl_scroll_serial ,cl_send_date 
                from student_profile, degree_main, convocation_listing
                where stud_degree = degree_id
                and stud_id = cl_stud_id ";
                //and rownum <= 10";
                //and cl_sem_grad = '193'
        if ($senate_date){
            $sql1 = $sql1 . " and cl_senate_date = '$senate_date'";
        }

        if ($program){
            $sql1 = $sql1 . " and ((stud_program = '$program' and '$program' <> 'ALL') or '$program' = 'ALL')";
        }

        if ($degree){
            //$sql = $sql . " and ((stud_degree = '$degree' and '$degree' <> 'ALL') or '$degree' = 'ALL')";
            $sql1 = $sql1 . " and ((stud_degree = '$degree' and '$degree' <> 'ALL') or '$degree' = 'ALL')";
        }

        if ($stud_ic){
            $sql1 = $sql1 . " and ((stud_ic_cur = '$stud_ic' and '$stud_ic' <> 'ALL') or '$stud_ic' = 'ALL')";
        }

        $sql1 = $sql1." order by stud_degree, stud_name";

        $result = $this->_db->fetchAll($sql1);
        $this->_db->closeConnection();
        return $result;

    }

    public function getConvoListGenExcel($senate_date = "",$program = "",$degree = "",$stud_ic = "",$cl_id="") {

        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $sql= " select upper(stud_name) stud_name, upper(degree_desc_short) degree_bi, upper(degree_desc_long) degree_bm,
                upper(cl_academicstatus) cl_academicstatus, upper(cl_academicstatus_eng) cl_academicstatus_eng,
                to_number(to_char(cl_senate_date,'DD'))||' '||rtrim(to_char(cl_senate_date,'Month','NLS_DATE_LANGUAGE=malay'))||' '||to_char(cl_senate_date,'YYYY') senate_date_bm,
                to_number(to_char(cl_senate_date,'DD'))||' '||rtrim(to_char(cl_senate_date,'Month'))||' '||to_char(cl_senate_date,'YYYY') senate_date_bi,
                cl_scroll_serial, cl_scroll_url
                from student_profile, degree_main, convocation_listing
                where stud_degree = degree_id
                and stud_id = cl_stud_id";
        //and cl_sem_grad = '193'
        if ($senate_date){
            $sql = $sql . " and cl_senate_date = '$senate_date'";
        }

        if ($program){
            $sql = $sql . " and ((stud_program = '$program' and '$program' <> 'ALL') or '$program' = 'ALL')";
        }

        if ($degree){
            $sql = $sql . " and ((stud_degree = '$degree' and '$degree' <> 'ALL') or '$degree' = 'ALL')";
        }

        if ($stud_ic){
            $sql = $sql . " and ((stud_ic_cur = '$stud_ic' and '$stud_ic' <> 'ALL') or '$stud_ic' = 'ALL')";
        }

        if ($senate_date){
            $sql = $sql . " and cl_senate_date = '$senate_date'";
        }

        if ($cl_id){
            $sql = $sql . " and cl_id = '$cl_id'";
        }

        $sql = $sql." order by stud_degree, stud_name";

        $result = $this->_db->fetchAll($sql);
        $this->_db->closeConnection();
        return $result;

    }

    public function getConvoListingForUpdate($type = "",$senate_date = "",$program="",$degree="",$stud_ic="") {

        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');


        $sql = $this->_db->select()
            ->from(array('C' => $this->_convolist))
            ->join(array('S' => $this->_profile),'C.CL_STUD_ID = S.STUD_ID', array('STUD_NAME'))
            ->join(array('D' => $this->_degree), 'D.DEGREE_ID = S.STUD_DEGREE',array('DEGREE_ID','DEGREE_DESC_LONG','DEGREE_LEVEL' =>'get_setup_info.get_degree_level(DEGREE_ID)'));

        $sql->where('C.CL_SENATE_DATE = ?',$senate_date);

        if ($program){
            $sql = $sql . " and ((stud_program = '$program' and '$program' <> 'ALL') or '$program' = 'ALL')";
        }

        if ($degree){
            $sql = $sql . " and ((stud_degree = '$degree' and '$degree' <> 'ALL') or '$degree' = 'ALL')";
        }

        if ($stud_ic){
            $sql = $sql . " and ((stud_ic_cur = '$stud_ic' and '$stud_ic' <> 'ALL') or '$stud_ic' = 'ALL')";
        }

        if ($type == "gen-url") {
            $sql = $sql . " and C.CL_SCROLL_URL IS NULL";
            //$sql->where(' and C.CL_SCROLL_URL IS NULL');
        }

        if ($type == "gen-serial"){
            $sql = $sql . " and C.CL_SCROLL_SERIAL IS NULL";
            //$sql->where(' and C.CL_SCROLL_SERIAL IS NULL');
        }

        if ($type == "update-date"){
            //update all ikut parameter
            //$sql = $sql . " and C.CL_SEND_DATE IS NULL";
        }

        $result = $this->_db->fetchAll($sql);
        $this->_db->closeConnection();
        return $result;

    }

    public function updateUrl($data, $id) {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $url = $data['CL_SCROLL_URL'];
        $sql = "UPDATE CONVOCATION_LISTING SET CL_SCROLL_URL = '$url' WHERE CL_ID = '$id'";

        //echo $sql;exit;

        $stmt = new Zend_Db_Statement_Oracle($this->_db, $sql);
        $stmt->execute();
        $this->_db->commit();


        $this->_db->closeConnection();


    }

    public function updateSerialNo($data, $id) {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $sn_no = $data['CL_SCROLL_SERIAL'];
        $sql = "UPDATE CONVOCATION_LISTING SET CL_SCROLL_SERIAL = '$sn_no' WHERE CL_ID = '$id'";

        //echo $sql;exit;

        $stmt = new Zend_Db_Statement_Oracle($this->_db, $sql);
        $stmt->execute();
        $this->_db->commit();


        $this->_db->closeConnection();

    }

    public function getSemList() {

        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $sql = "select distinct cl_sem_grad from convocation_listing order by cl_sem_grad desc";

        $result = $this->_db->fetchAll($sql);
        $this->_db->closeConnection();
        return $result;
        Zend_Debug::dump($stmt);
    }

    public function getSenateDate() {

        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $sql = "select to_char(cl_senate_date,'yyyy-mm-dd') senate_date,cl_senate_date
                from convocation_listing
                group by to_char(cl_senate_date,'yyyy-mm-dd'),cl_senate_date
                order by senate_date desc ";

        $result = $this->_db->fetchAll($sql);
        $this->_db->closeConnection();
        return $result;
        Zend_Debug::dump($stmt);
    }

    public function getDegreeList($senate_date,$program_id) {

        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $sql = "select degree_id,degree_id||' - '||degree_desc_long as degree_desc_long
                from convocation_listing, student_profile, degree_main
                where cl_stud_id = stud_id
                and stud_degree = degree_id
                --and to_char(cl_senate_date,'yyyy-mm-dd') = <senate_date>
                and cl_senate_date = '$senate_date'
                --and ((stud_program = '$program_id' and '$program_id' <> 'ALL') or '$program_id' = 'ALL')
                and ((stud_program = '$program_id' and '$program_id' <> 'ALL') or '$program_id' = 'ALL')
                group by degree_id,degree_desc_long
                order by degree_id";
//echo $sql;
        $result = $this->_db->fetchAll($sql);
        $this->_db->closeConnection();
        return $result;
        Zend_Debug::dump($stmt);
    }

    public function getProgramList($senate_date) {

        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $sql = "select prog_id,prog_id||' - '||prog_desc as prog_desc
                from convocation_listing, program, student_profile
                where cl_stud_id = stud_id
                and stud_program = prog_id
                --and to_char(cl_senate_date,'yyyy-mm-dd') = '2020-09-03'
                and cl_senate_date = '$senate_date'
                group by prog_id, prog_desc
                order by prog_id";
//echo $sql;
        $result = $this->_db->fetchAll($sql);
        $this->_db->closeConnection();
        return $result;
        Zend_Debug::dump($stmt);
    }


    public function genSerialNo ($pstud_id){

        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $sql = 'declare begin :sts :=exam4.getScroll_serialNo(:pstud_id); end;';

        $stmt = $this->_db->prepare($sql);

        $stmt->bindParam(':pstud_id',$pstud_id,null,32);

        // Bind the output parameter
        $stmt->bindParam(':sts',$sts,null,100);

        // execute the PL/SQL code
        $stmt->execute();
        //Zend_Debug::dump($stmt);

        $this->_db->closeConnection();
        return $sts;
    }

    public function getConvoDataById($cl_id = "") {

        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $sql= " select cl_id,cl_sem_grad, stud_id, stud_ic_cur, upper(stud_name) stud_name, stud_program, stud_degree,
                upper(degree_desc_short) degree_desc,
                cl_academicstatus_eng,
                to_char(cl_senate_date,'DD')||' '||rtrim(to_char(cl_senate_date,'Month'))||' '||to_char(cl_senate_date,'YYYY') senate_date,
                decode(nvl(cl_scroll_serial,'No'),'No','No','Yes') serial_no_sts,
                decode(nvl(cl_scroll_url,'No'),'No','No','Yes') scroll_url_sts,cl_scroll_serial ,cl_scroll_url 
                from student_profile, degree_main, convocation_listing
                where stud_degree = degree_id
                and stud_id = cl_stud_id ";
        //and rownum <= 10";
        //and cl_sem_grad = '193'
        if ($cl_id){
            $sql = $sql . " and cl_id = '$cl_id'";
        }

        $result = $this->_db->fetchAll($sql);
        $this->_db->closeConnection();
        return $result;

    }

    public function getUsername($ic = "") {

        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $sql= "select emp_username from employee_main where emp_new_ic = '$ic' and emp_username is not null";


        $result = $this->_db->fetchRow($sql);
        $this->_db->closeConnection();
        return $result;

    }

    public function insertHistory($data) {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $today = date('Y-m-d H:i:s');
        $cl_id = $data['SSH_CL_ID'];
        $new_serialNo = $data['SSH_NEW_SERIAL_NO'];
        $old_serialNo = $data['SSH_OLD_SERIAL_NO'];
        $old_url = $data['SSH_OLD_URL_SCROLL'];
        $newurl = $data['SSH_NEW_URL_SCROLL'];
        $username = $data['SSH_CREATE_BY'];

        $sql = "insert into scroll_serial_his values 
                (scroll_serial_his_seq.nextval, '$cl_id', '$new_serialNo', '$old_serialNo',
                '$newurl', '$old_url', sysdate, '$username')";

        //echo $sql;exit;

        $stmt = new Zend_Db_Statement_Oracle($this->_db, $sql);
        $stmt->execute();
        $this->_db->commit();


        $this->_db->closeConnection();


    }

    public function updateNewSnURLById($data, $id) {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $new_serialNo = $data['SSH_NEW_SERIAL_NO'];
        $newurl = $data['SSH_NEW_URL_SCROLL'];

        $sql = "UPDATE CONVOCATION_LISTING SET CL_SCROLL_SERIAL = '$new_serialNo', CL_SCROLL_URL='$newurl' WHERE CL_ID = '$id'";

        //echo $sql;exit;

        $stmt = new Zend_Db_Statement_Oracle($this->_db, $sql);
        $stmt->execute();
        $this->_db->commit();


        $this->_db->closeConnection();

    }

    public function updateNewUrlById($data, $id) {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $new_serialNo = $data['SSH_NEW_SERIAL_NO'];
        $newurl = $data['SSH_NEW_URL_SCROLL'];

        $sql = "UPDATE CONVOCATION_LISTING SET CL_SCROLL_URL='$newurl' WHERE CL_ID = '$id'";

        //echo $sql;exit;

        $stmt = new Zend_Db_Statement_Oracle($this->_db, $sql);
        $stmt->execute();
        $this->_db->commit();


        $this->_db->closeConnection();

    }

    public function updateDateSirimById($data, $id) {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $send_date = $data['CL_SEND_DATE'];


        $sql = "UPDATE CONVOCATION_LISTING SET CL_SEND_DATE = TO_DATE('$send_date', 'dd-mm-yy') WHERE CL_ID = '$id'";


        $stmt = new Zend_Db_Statement_Oracle($this->_db, $sql);
        $stmt->execute();
        $this->_db->commit();


        $this->_db->closeConnection();

    }

}
