<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initAutoload() {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->setFallbackAutoloader(true);
        return $autoloader;
    }

    protected function _initConfig() {
        $config = new Zend_Config($this->getOptions());
        Zend_Registry::set('config', $config);
    }

    protected function _initRewrite() {
        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/routes.ini', 'routers');
        $router->addConfig($config);
    }

    protected function _initDb() {
        $resource = $this->getPluginResource('multidb');
        Zend_Registry::set("multidb", $resource);
    }

    protected function _initBaseUrl() {
        $options = $this->getOptions();
        $baseUrl = isset($options['settings']['baseUrl']) ? $options['settings']['baseUrl'] : null;  // null tells front controller to use autodiscovery, the default
        $this->bootstrap('frontcontroller');
        $front = $this->getResource('frontcontroller');
        $front->setBaseUrl($baseUrl);
    }

}
