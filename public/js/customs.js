$(document).ready(function() {

    // Tooltip
    $('#icNum').tooltip({
        'trigger':'hover',
        'title': 'Please Enter Your Full I.C Number',
        'placement':'top'});

    $('#search').attr('disabled', true);

    $('#icNum').on('keyup', function() {
        //this.value = this.value.replace(/[^0-9\.]/,'');
        if ($(this).val() !== '') {
            $('#search').attr('disabled', false);
        } else {
            $('#search').attr('disabled', true);
        }
    });


    // $("#icNum").on("keyup", function(){
    //     this.value = this.value.replace(/[^0-9\.]/g,'');
    //     var n = this.value.length;
    //     if(n < 12 || n > 12){
    //         $("#search").prop("disabled", true);
    //     } else {
    //         $("#search").prop("disabled", false);
    //     }
    // });
    
});